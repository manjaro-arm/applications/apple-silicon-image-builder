#!/bin/sh
set -e

mkdir -p "/boot/efi"

pacman --noconfirm -Syu
pacman --noconfirm -S asahi-scripts asahi-fwextract sed uboot-apple-silicon mkinitcpio grub iwd systemd-sysvcompat
